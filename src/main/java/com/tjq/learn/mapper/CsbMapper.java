package com.tjq.learn.mapper;

import com.tjq.learn.pojo.Csb;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * @author TJQ
 * @data 2020/3/4 19:08
 */
@Mapper
public interface CsbMapper {

    @Select("select * from csb")
    List<Csb> showAll();

    @Update("update csb set cs2=#{cs2},cs3=#{cs3} where cs1=#{cs1}")
    int updCsb(Csb csb);

    @Delete("delete from csb where cs1=#{cs1}")
    void delCsb(String cs1);

    @Insert("insert into csb(cs1,cs2,cs3) values(#{cs1},#{cs2},#{cs3})")
    int addCsb(Csb csb);

}
