package com.tjq.learn.controller;

import com.tjq.learn.pojo.Csb;
import com.tjq.learn.service.CsbService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author TJQ
 * @data 2020/3/3 22:10
 */
@RestController
@CrossOrigin
@RequestMapping("/demo")
public class DemoController {

    @Autowired
    CsbService csbService;

    @GetMapping("/showAll")
    public List<Csb> helloWorld(){
        return csbService.showAll();
     }

}
