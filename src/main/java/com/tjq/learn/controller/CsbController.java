package com.tjq.learn.controller;

import com.tjq.learn.pojo.Csb;
import com.tjq.learn.service.CsbService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author TJQ
 * @data 2020/3/4 19:08
 */
@RestController
@CrossOrigin
@RequestMapping("/csb")
public class CsbController {

    @Autowired
    CsbService csbService;

    @GetMapping("/showCsb")
    public Map<String, Object> showCsb(){
        Map<String, Object> map = new HashMap<>();
        map.put("code", 0);
        map.put("msg", ".....");
        map.put("data", csbService.showAll());
        return map;
    }

    @PostMapping("/updCsb")
    public int updCsb(Csb csb) {
        return csbService.updCsb(csb);
    }

    @DeleteMapping("/delCsb/{cs1s}")
    public boolean delCsb(@PathVariable("cs1s") String[] cs1s) {
        return csbService.delCsb(cs1s);
    }

    @PostMapping("/addCsb")
    public int addCsb(Csb csb){
        System.out.println(csb.getCs1());
        System.out.println("aaa");
        return csbService.addCsb(csb);
    }

}
