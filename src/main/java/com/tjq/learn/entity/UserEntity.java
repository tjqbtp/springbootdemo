package com.tjq.learn.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author TJQ
 * @data 2020/3/17 21:10
 */
@Data
public class UserEntity implements Serializable {

    private Long id;
    private String guid;
    private String name;
    private String age;
    private Date createTime;

}
