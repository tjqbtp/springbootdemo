package com.tjq.learn.service;

import com.tjq.learn.mapper.CsbMapper;
import com.tjq.learn.pojo.Csb;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author TJQ
 * @data 2020/3/4 19:10
 */
@Service
public class CsbService {


    @Autowired
    CsbMapper csbMapper;

    public List<Csb> showAll(){
        return csbMapper.showAll();
    }

    public int updCsb(Csb csb){
        return csbMapper.updCsb(csb);
    }

    public boolean delCsb(String[] cs1s){
        try {
            for(String cs1 : cs1s) {
                csbMapper.delCsb(cs1);
            }
            return true;
        }catch(Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public int addCsb(Csb csb){
        return csbMapper.addCsb(csb);
    }

}
