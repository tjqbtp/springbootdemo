package com.tjq.learn.pojo;

import lombok.Data;

/**
 * @author TJQ
 * @data 2020/3/4 19:10
 */
@Data
public class Csb {

    private String cs1;
    private String cs2;
    private String cs3;

}
